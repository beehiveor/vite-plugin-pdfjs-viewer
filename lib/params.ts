import type { ServeStaticOptions } from "serve-static";

export interface PdfJsViewerPluginOptions {
  /**
   * Prefix URL for viewer.
   */
  base?: string,

  /**
   * Path to `pdfjs-viewer-build`. Default is `./node_modules/pdfjs-viewer-build/viewer/`.
   */
  build?: string,

  /**
   * Path for output. Default is `vite.config.build.outDir`.
   */
  out?: string,

  /**
   * Use default PDFJS index.html. Default is `false`.
   */
  index?: boolean;

  /**
   * Options passed to [`serveStatic`](https://github.com/expressjs/serve-static#options)
   * dev server middleware.
   * 
   * WARN: `options.index` is ommited.
   */
  serve?: Omit<ServeStaticOptions, "index">,
}
