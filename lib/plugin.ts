import path, { posix } from "path";
import { promises as fs } from "fs";
import serveStatic from "serve-static";
import { PluginOption } from "vite";

import { copyFilesRecursive, injectHTML } from "./utils";

import type { PdfJsViewerPluginOptions } from "./params";

export function pdfViewer(options?: PdfJsViewerPluginOptions): PluginOption {
  const buildDir = path.resolve(options?.build || "./node_modules/pdfjs-viewer-build/viewer/");
  const prefix = posix.join("/", options?.base || "", "/");

  const indexName = "index.html";
  const indexBuildPath = path.join(buildDir, indexName);

  const exclude = [ ".js.map", ".css.map", '.pdf' ];

  if (options?.index == false) {
    exclude.push(indexName);
  }

  let outDir: string;
  let sourcePromise: Promise<string> = Promise.resolve(""); 

  const testHtmlPath = (p: string) => posix.join(posix.dirname(p), "/") === prefix;

  return {
    name: "vite-plugin-pdfjs-viewer",

    configResolved(config) {
      // console.debug("configResolved", { outDir: config.build.outDir });
      outDir = options?.out || path.join(config.build.outDir, prefix);
    },

    buildStart() {
      sourcePromise = fs.readFile(indexBuildPath, { encoding: "utf8" });
    },

    async transformIndexHtml(html, ctx): Promise<string> {
      let out = "";
      // console.debug("transformIndexHtml", ctx);
      // console.debug(html);

      if (testHtmlPath(ctx.path) && (out = await sourcePromise)) {
        out = injectHTML(out, html, "head");
        out = injectHTML(out, html, "body");
      }

      return out || html;
    },

    configureServer(server) {
      const route = posix.join("/", server.config.base, prefix);
      // console.debug("configureServer", { route });
      server.middlewares.use(route, serveStatic(buildDir, {
        ...options?.serve,
        index: options?.index === false ? false : undefined,
      }));
    },

    async closeBundle() {
      await copyFilesRecursive(buildDir, outDir, exclude);
    }
  }
}

export default pdfViewer;