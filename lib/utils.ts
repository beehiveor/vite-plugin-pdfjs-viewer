
import * as path from "path";
import * as fs from "fs/promises";

export async function copyFilesRecursive(
    src: string,
    dst: string,
    exclude?: string[],
): Promise<void> {
  const files = await getFiles(src);

  await Promise.all(files
    .filter(p => exclude ? (exclude.find(end => p.endsWith(end)) ? false : true) : true)
    .map(async (file: string) => {
      const src_ = path.relative(src, file);
      const dst_ = path.join(dst, src_);

      await fs.mkdir(path.dirname(dst_), { recursive: true });
      await fs.copyFile(file, dst_);
  }));
}

async function getFiles(src: string = "./"): Promise<string[]> {
  const entries = await fs.readdir(src, { withFileTypes: true });

  const files = entries
    .filter(file => !file.isDirectory())
    .map(file => path.join(src, file.name));

  const folders = entries.filter(folder => folder.isDirectory());

  await Promise.all(folders.map(async folder => {
    const innerSrc = path.join(src, folder.name);
    const innerFiles = await getFiles(innerSrc);
    files.push(...innerFiles);
  }));

  return files;
}

export function injectHTML(dst: string, src: string, target: "head" | "body"): string {
  const regexes = {
    "head": /<head>(.*?<\/head>)/s,
    "body": /<body>(.*?<\/body>)/s,
  }

  const search = `</${target}>`;

  return dst.replace(search, src.match(regexes[target])?.[1] || search);
}
