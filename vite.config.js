import { defineConfig } from "vite";
import pdfViewer from ".";

export default defineConfig({
  plugins: [
    pdfViewer({
      serve: {
        index: false,
      }
    }),
  ],
  build: {
    outDir: "./public",
  },
  publicDir: "./static",
})